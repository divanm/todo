﻿using Microsoft.AspNetCore.Blazor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Productivity.ToDo.UI.Blazor.Client
{
    public class ApiClient
    {
        const string BaseUrl = "https://localhost:44334/api/v2.0"; //TODO: Config

        private readonly HttpClient _httpClient;

        public ApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<ToDoResponse>> GetToDoListAsync()
        {
            var response = await _httpClient.GetJsonAsync<IEnumerable<ToDoResponse>>($"{BaseUrl}/ToDoItems");
            return response;
        }

        public async Task<HttpResponseMessage> CreateToDoAsync(string text)
        {
            var response = await _httpClient.PostAsync((string.Format("{0}/ToDoItems?text={1}", BaseUrl, text)), null);
            return response;
        }
    }
}
