﻿using System;

namespace Productivity.ToDo.UI.Blazor.Client
{
    public class ToDoResponse
    {
        public bool done { get; set; }
        public string text { get; set; }
        public Guid id { get; set; }
    }
}