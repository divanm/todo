﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Productivity.ToDo.API.Controllers.V2
{
    [EnableCors("DoorsWideOpenPolicy")]
    [ApiVersion("2.0")]
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]
    public class ImportanceController : ControllerBase
    {
       
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]List<Guid> ids)
        {
            var importanceInfo = new List<ImportanceInfoView>();

            var importanceList = new List<string> { "Very Important!", "Maybe Later?", "Get someone else to do it."};

            Random r = new Random();

            foreach (var i in ids)
            {
                int index = r.Next(importanceList.Count);
                var importance = importanceList[index];

                importanceInfo.Add(new ImportanceInfoView { Id = i, Info = importance });
            }

            return Ok(importanceInfo);
       }
    }

    public class ImportanceInfoView
    {
        public Guid Id { get; set; }
        public string Info { get; set; }

    }
}