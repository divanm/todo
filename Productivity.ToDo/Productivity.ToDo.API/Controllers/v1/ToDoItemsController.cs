﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Productivity.ToDo.Data;
using Productivity.ToDo.Shared;

namespace Productivity.ToDo.API.Controllers.V1
{
    [EnableCors("DoorsWideOpenPolicy")]
    [ApiVersion("1.0")]
    [Produces("application/json")]
    [Route("api/v{api-version:apiVersion}/[controller]")]
    [ApiController]
    public class ToDoItemsController : ControllerBase
    {
        private readonly ToDoContext _context;

        public ToDoItemsController(ToDoContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var todos = await _context.ToDoItems
            .OrderBy(t => t.Done)
            .ThenBy(t => t.Text).ToListAsync();

            var viewItems = todos.Select(
                t => new ToDoView
                { Created = t.Created, Done = t.Done, Id = t.Id, Text = t.Text });

            return Ok(viewItems);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var t = await _context.ToDoItems.SingleOrDefaultAsync(i => i.Id == id);

            if (t != null)
            {
                return Ok(new ToDoView
                { Created = t.Created, Done = t.Done, Id = t.Id, Text = t.Text });
            }

            return NotFound(id);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] string text)
        {
            var newTodo = new ToDoItem { Created = DateTime.UtcNow, Id = Guid.NewGuid(), Text = text };

            _context.Add(newTodo);
            await _context.SaveChangesAsync();

            var created = new ToDoView { Created = newTodo.Created, Id = newTodo.Id, Text = newTodo.Text };

            return Created(Url.Link("v1", new { controller = "ToDoItems", id = created.Id }), created);
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] ToDoView todo)
        {
            var t = await _context.ToDoItems.SingleOrDefaultAsync(i => i.Id == todo.Id);

            if (t != null)
            {
                t.Text = todo.Text;
                t.Done = todo.Done;

                await _context.SaveChangesAsync();

                return Ok(new ToDoView
                { Created = t.Created, Done = t.Done, Id = t.Id, Text = t.Text });
            }

            return NotFound(todo.Id);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(Guid id)
        {
            var t = await _context.ToDoItems.SingleOrDefaultAsync(i => i.Id == id);

            if (t != null)
            {
                _context.Remove(t);
                await _context.SaveChangesAsync();

                return Ok(id);
            }

            return NotFound(id);
        }

        //TODO - Purge and all as done
    }
}