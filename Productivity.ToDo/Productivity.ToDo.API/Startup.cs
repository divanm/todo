﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Productivity.ToDo.Data;
using SwashbuckleAspNetVersioningShim;
using System;

namespace Productivity.ToDo.API
{
    public class Startup
    {
        private IHostingEnvironment CurrentEnvironment { get; set; }

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            CurrentEnvironment = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddCors(options =>
            //{
            //    options.AddPolicy("AllowSpecificOrigin", builder => builder.WithOrigins("https://localhost:44351/").AllowAnyMethod().AllowAnyHeader()); //TODO: Config                 
            //});

            services.AddCors(o => o.AddPolicy("DoorsWideOpenPolicy", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader()
                       .AllowCredentials();
            }));

            services.AddDbContext<ToDoContext>(opt => opt.UseInMemoryDatabase());

            services.AddRouting();
            services.AddMvc();
            services.AddMvcCore().AddVersionedApiExplorer();
            services.AddApiVersioning();

            services.AddSwaggerGen(c =>
            {
                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();
                c.ConfigureSwaggerVersions(provider);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider, IApiVersionDescriptionProvider provider)
        {
            var context = serviceProvider.GetService<ToDoContext>();
            context.SeedTestData(); // TODO: Could move this inside env.IsDevelopment() but the in-memory DB is getting wiped between app pool recycles so leaving it here for this demo

            app.UseCors("DoorsWideOpenPolicy");
           
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.ConfigureSwaggerVersions(provider);
            });
        }
    }
}