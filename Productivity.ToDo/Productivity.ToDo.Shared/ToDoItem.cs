﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Productivity.ToDo.Shared
{
    public class ToDoItem: IEntity
    {
        public Guid Id { get; set; }
        [Required]
        public string Text { get; set; }
        public DateTime Created { get; set; }
        public bool Done { get; set; }

        //public ToDoUser ToDoUser { get; set; }
        //public Guid ToDoUserId { get; set; }
        //public Guid UserId { get; set; }

    }
}