﻿using System;

namespace Productivity.ToDo.Shared
{
    public class ToDoView
    {
        public Guid Id { get; set; }      
        public string Text { get; set; }
        public DateTime Created { get; set; }
        public bool Done { get; set; }
    }
}