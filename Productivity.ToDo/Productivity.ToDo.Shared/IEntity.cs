﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Productivity.ToDo.Shared
{
    public interface IEntity
    {
        [Key]
        Guid Id { get; set; }
    }
}