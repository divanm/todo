﻿using Microsoft.AspNetCore.Routing;

namespace Productivity.ToDo.API.Ops
{
    public interface IRouteInterceptor
    {
        bool Matches(RouteData routeData, string httpMethod);
    }
}