﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using System.Threading.Tasks;

namespace Productivity.ToDo.API.Ops
{
    public interface IViewModelAppender : IRouteInterceptor
    {
        Task Append(dynamic viewModel, RouteData routeData, IQueryCollection query);
    }
}