﻿using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

namespace Productivity.ToDo.API.Ops
{
    static class TaskExtensions
    {
        public static async Task WithLogging(this Task task, Func<ILogger> createLogger)
        {
            try
            {
                await task;
            }
            catch (Exception ex)
            {
                createLogger().LogError(ex.ToString());
                throw;
            }
        }
    }

   

    public static class MvcBuilderExtensions
    {
        public static IMvcBuilder AddViewModelCompositionMvcSupport(this IMvcBuilder builder)
        {
            builder.Services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(typeof(CompositionFilter));
            });

            return builder;
        }
    }
}