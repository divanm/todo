﻿using Microsoft.EntityFrameworkCore;
using Productivity.ToDo.Shared;
using System;
using System.Collections.Generic;

namespace Productivity.ToDo.Data
{
    public class ToDoContext : DbContext
    {
        public ToDoContext(DbContextOptions<ToDoContext> options)
           : base(options)
        {
        }

        public DbSet<ToDoItem> ToDoItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<ToDoUser>()
            // .HasMany(u => u.ToDoItems)
            // .WithOne(t => t.ToDoUser)
            // .HasForeignKey(p => p.ToDoUserId);
        }

        public void SeedTestData()
        {
            var seedItems = new List<ToDoItem>
            {
                new ToDoItem { Created = DateTime.UtcNow, Id = Guid.NewGuid(), Text = "A"},
                new ToDoItem { Created = DateTime.UtcNow, Id = Guid.NewGuid(), Text = "B"},
                new ToDoItem { Created = DateTime.UtcNow, Id = Guid.NewGuid(), Text = "C"},
                new ToDoItem { Created = DateTime.UtcNow, Id = Guid.NewGuid(), Text = "D"},
                new ToDoItem { Created = DateTime.UtcNow, Id = Guid.NewGuid(), Text = "E"}
            };

            ToDoItems.AddRange(seedItems);

            SaveChanges();
        }
    }
}